(function validation() {

	 var inputs = "[type='text'], [type='password'], [type='date'], [type='datetime'], [type='datetime-local'], [type='month'], [type='week'], [type='email'], [type='number'], [type='search'], [type='tel'], [type='time'], [type='url'], [type='color'], textarea";


    $(inputs).change(function () {
        if ($(this).val().length > 0) {
            $(this).addClass("valid"); 
        } else {
        	$(this).removeClass("valid"); 
        }

        var valid = [];
        var all = [];

        $(this).parent().find("input, textarea").each(function () {

        	if ($(this).attr("type") != "file" && $(this).attr("type") != "checkbox" && $(this).attr("type") != "radio") {
        		all.push($(this))

        		$(this).hasClass("valid") ? valid.push($(this)) : ""; 
        	}
        });	

        if (valid.length === all.length) {
            $(this).parent().parent().find('.button').prop("disabled", false);
        } else {
 			$(this).parent().parent().find('.button').prop("disabled", true);
        }
    });
 
})();