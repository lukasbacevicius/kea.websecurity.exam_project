var cacheList = [
	'/resources/js/app.js',
	'/resources/css/app.css',
	'/resources/css/fonts.css'
];

//= include ../bower_components/sw-toolbox/sw-toolbox.js

toolbox.options.debug = false;

toolbox.precache(cacheList);

toolbox.router.default = toolbox.fastest;

toolbox.router.any('/', toolbox.fastest);
toolbox.router.post('/*', toolbox.networkOnly);

toolbox.router.any('/serviceworker.js', toolbox.networkOnly);