<?php 
	include "header.php";
?>
<div class="callout">
  <div class="row column text-center">
      <h1>Login page</h1>
  </div>
</div>
     
      <?php
      $two_step_login_active = false;
      if(\WebSecurityExam\Auth::twoStepLogin() === false && isset($_POST['action_login'])){
        $identification = $_POST['login'];
        $password = $_POST['password'];
        if($identification == "" || $password == ""){
          echo '<div class="large-4 columns large-centered"><p class="alert callout">Username or Password left blank !</p></div>';
        }else{
          $login = \WebSecurityExam\Auth::twoStepLogin($identification, $password, isset($_POST['remember_me']));
          if($login === false){
            echo '<div class="large-4 columns large-centered"><p class="alert callout">Username or Password Wrong</p></div>';
          }else if(is_array($login) && $login['status'] == "blocked"){
            echo '<div class="large-4 columns large-centered"><p class="alert callout">Too many login attempts. You are blocked from logging in for '. $login['minutes'] .' minutes ('. $login['seconds'] .' seconds)</p></div>';
          }else{
            $two_step_login_active = true;
          }
        }
      }
      if(!$two_step_login_active){
      ?>
        <form action="login.php" method="POST">
        <div class="row">
        <div class="large-5 columns large-centered">
        <div class="callout secondary">
            <label>
              Username / E-Mail</p>
              <input name="login" type="text"/>
            </label>    
            <label>
              Password</p>
              <input name="password" type="password"/>
            </label>     
            <label>
                <input type="checkbox" name="remember_me" id="remember_me" />
                <label for="remember_me">Remember Me</label>
            </label>
             <button class="button large text-center" name="action_login">Log In</button> 
          </div>
        </div>
        </div>
        </form>
  
      <?php
      }
      ?>
      <div class="row">
        <div class="large-10 large-centered columns text-center">
        <a class="button large hollow" href="register.php">Register</a>
      
        <a class="button large hollow" href="reset.php">Reset Password</a>
    </div>
    </div>


<?php include "footer.php"; ?>