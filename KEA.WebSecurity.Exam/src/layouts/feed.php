<?php 
	require "Class.feed.php";

	\WebSecurityExam\Feed::init(); 

	$posts = \WebSecurityExam\Feed::getFeed(); 

	$user = \WebSecurityExam\Auth::getUser();

	if (isset($_POST['statusPost'])) {	
	\WebSecurityExam\Feed::post($user['id'], $_POST['content'], $_FILES['userimage']['name'], $_FILES['userimage']['tmp_name'], $_FILES['userimage']['size']);
		$posts = \WebSecurityExam\Feed::getFeed(); 
	}

?>
<div class="row">
<div class="large-8 columns large-centered">
	<div class="callout status-update secondary">
		<form action="home.php" method="POST" enctype="multipart/form-data">
          	<textarea rows="2" name="content" placeholder="What's on your mind?"></textarea>
          	<input type="file" accept="image/*" name="userimage" placeholder="Upload Image" id="userimageinput">
          	<label class="button hollow" for="userimageinput">Select image to upload</label>
			<button type="submit" class="button large" name="statusPost">Post</button>
		</form>
		<hr>
		<ul class="posts no-bullet">

<?php
	if(count($posts) == 0) {
		echo '<div class="large-4 columns large-centered"><p class="alert text-center callout">No posts</p></div>';
	} else {
		arsort($posts);
		foreach($posts as $post) {

			$user = \WebSecurityExam\Auth::getUser("*", $post['author_id']);

			$date = new DateTime($post['date_created']);
			$date = $date->format('Y-m-d');

			if (empty($post['image'])) {
				echo "<li><div class='status-update__box'><div class='content'> <div class='header'> <div class='name'><h5>{$user['name']} ({$user['email']})</h5> </div><span class='sub'>{$date}</span></div></div><div class='text'>{$post['content']}</div></div></li>";
			} else {
				echo "<li><div class='status-update__box'><div class='content'> <div class='header'> <div class='name'><h5>{$user['name']} ({$user['email']})</h5> </div><span class='sub'>{$date}</span></div></div><div class='text'>{$post['content']}</div><div class='thumbnail'><img src='uploads/{$post['image']}'></div></div></li>";
			}
		}
	}
 ?>

	</ul>
	</div>
</div>
</div>