<?php 
	include "header.php";
?>

<div class="callout">
    <div class="row column text-center">
    <h1>Change password</h1>
  </div>
</div>

<?php
if(isset($_POST['change_password'])){
  if(isset($_POST['current_password']) && $_POST['current_password'] != "" && isset($_POST['new_password']) && $_POST['new_password'] != "" && isset($_POST['retype_password']) && $_POST['retype_password'] != "" && isset($_POST['current_password']) && $_POST['current_password'] != ""){
      
    $curpass = $_POST['current_password'];
    $new_password = $_POST['new_password'];
    $retype_password = $_POST['retype_password'];
      
    if($new_password != $retype_password){
      echo '<div class="large-4 columns large-centered"><p class="alert callout">New passwords do not match</p></div>';
    }else if(\WebSecurityExam\Auth::login(\WebSecurityExam\Auth::getUser("username"), $_POST['current_password'], false, false) == false){
      echo '<div class="large-4 columns large-centered"><p class="alert callout">Incorrect current password</p></div>';
    }else{
      $change_password = \WebSecurityExam\Auth::changePassword($new_password);
      if($change_password === true){
        echo '<div class="large-4 columns large-centered"><p class="success callout">Password changed successfully</p></div>';
      }
    }
  }else{
 echo '<div class="large-5 columns large-centered"><p class="alert callout">Some fields left blank</p></div>';
  }
}
?>

<form action="<?php echo \WebSecurityExam\Auth::curPageURL();?>" method='POST'>
  <div class="row">
  <div class="large-5 columns large-centered">
  <div class="callout secondary">
  <label>
    Current Password
    <input type='password' name='current_password' />
  </label>
  <label>
    New Password
    <input type='password' name='new_password' />
  </label>
  <label>
    Repeat New Password
    <input type='password' name='retype_password' />
  </label>
  <button class="button large text-center" name='change_password' type='submit'>Change Password</button>
  </div>
  </div>
  </div>
</form>

  <div class="row">
      <div class="large-10 large-centered columns text-center">
      <a class="button large hollow" href="home.php">Back to home page</a>
    </div>
    </div>

<?php include "footer.php"; ?>