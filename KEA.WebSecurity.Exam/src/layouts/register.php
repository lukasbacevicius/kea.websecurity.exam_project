<?php 
	include "header.php";
?>

<div class="content">
<div class="callout">
  <div class="row column text-center">
      <h1>Register</h1>
  </div>
</div>


    <?php
  if( isset($_POST['submit']) ){
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['pass'];
    $retyped_password = $_POST['retyped_password'];
    $name = $_POST['name'];
    if( $username == "" || $email == "" || $password == '' || $retyped_password == '' || $name == '' ){
        echo '<div class="large-5 columns large-centered"><p class="alert callout">Some Fields were left blank.</p></div>';
    }elseif( !\WebSecurityExam\Auth::validEmail($email) ){
        echo '<div class="large-5 columns large-centered"><p class="alert callout">E-mail address provided is not valid</p></div>';
    }elseif( !ctype_alnum($username) ){
        echo '<div class="large-5 columns large-centered"><p class="alert callout">The username provided is invalid</p></div>';
    }elseif($password != $retyped_password){
        echo '<div class="large-5 columns large-centered"><p class="alert callout">Passwords do not mathc</p></div>';
    }else{
      $createAccount = \WebSecurityExam\Auth::register($username, $password,
        array(
          "email" => $email,
          "name" => $name,
          "created" => date("Y-m-d H:i:s") // Just for testing
        )
      );
      if($createAccount === "exists"){
        echo '<div class="large-5 columns large-centered"><p class="alert callout">User Exists.</p></div>';
      }elseif($createAccount === true){
        echo '<div class="large-5 columns large-centered"><p class="success callout">Acount was successfully created. <a href="login.php">Log In here</a></p></div>';
      }
    }
  }
  ?>


  <form action="register.php" method="POST">
    <div class="row">
    <div class="large-5 columns large-centered">
    <div class="callout secondary">
    <label>
      <input name="username" type="text" placeholder="Username" />
    </label>
    <label>
      <input name="email" type="email" placeholder="E-Mail" /> 
    </label>
    <label>
      <input name="pass" type="password" placeholder="Password" />
    </label>
    <label>
      <input name="retyped_password" type="password" placeholder="Repeat Password" />
    </label>
    <label>
      <input type="text" name="name" placeholder="Name" />
    </label>
      <button class="button large text-center" name="submit">Register</button>
      </div>
    </div>
    </div>
  </form>

    <div class="row">
      <div class="large-10 large-centered columns text-center">
      <a class="button large hollow" href="login.php">Back to login page</a>
    </div>
    </div>
</div>

<?php include "footer.php"; ?>