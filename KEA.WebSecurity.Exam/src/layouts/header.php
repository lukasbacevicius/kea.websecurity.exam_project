<?php 
	ob_start();
	require "Class.auth.php";

	\WebSecurityExam\Auth::init();
	
?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="da-DK"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="da-DK"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="da-DK"> <![endif]-->
<!--[if gt IE 8]><!-->

<html lang="da-DK">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="robots" content="noindex,nofollow" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title><?php include_once("title_selector.php"); ?> | Web security exam project</title>
    <link rel="stylesheet" href="assets/web-security-exam-project/css/app.css" />
     <noscript data-font-main>
        <link rel="stylesheet" href="assets/web-security-exam-project/css/fonts.css" />
    </noscript>
    <script>
        !function (e) { e._fontsLoaded = !1, e._fontsPreferred = !1; var t = navigator.userAgent.toLowerCase(), n = function (t) { return e._fontsLoaded = t }, r = function () { var e, t = navigator.userAgent, n = t.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; return /trident/i.test(n[1]) ? (e = /\brv[ :]+(\d+)/g.exec(t) || [], { name: "IE", version: e[1] || "" }) : "Chrome" === n[1] && (e = t.match(/\bOPR\/(\d+)/), null != e) ? { name: "Opera", version: e[1] } : (n = n[2] ? [n[1], n[2]] : [navigator.appName, navigator.appVersion, "-?"], null != (e = t.match(/version\/(\d+)/i)) && n.splice(1, 1, e[1]), { name: n[0], version: n[1] }) }(), o = function (e) { "loading" != document.readyState ? e() : document.addEventListener("DOMContentLoaded", e) }, a = e._fontsFallback = function () { n(!0), o(function () { var e = document.querySelector("[data-font-main]"); return e ? (e.outerHTML = e.innerHTML.replace("&lt;", "<").replace("&gt;", ">"), void (document.querySelector("html").className += " fonts-ready")) : void alert("no main el") }) }, f = e._supportsWoff = function () { if ("FontFace" in e) try { var n = new e.FontFace("t", 'url("data:application/font-woff,") format("woff")', {}); if (n.load()["catch"](function () { }), "loading" === n.status) return !0 } catch (o) { } if (!/edge\/([0-9]+)/.test(t)) { var a = /(chrome|firefox)\/([0-9]+)/.exec(t), f = { chrome: 36, firefox: 39 }; if (a) return !!a && f[a[1]] < parseInt(a[2], 10); if ("Safari" == r.name) return r.version >= 6 } return !1 }, i = !1; if (f()) i = "woff"; else if (i = "ttf", t.indexOf("android") > -1) return a(); if (e._fontsPreferred = i, "android" == i || location.hostname.indexOf("kunder.cabana.dk") > -1) return e._fontsPreferred = "css", void a(); if (!localStorage) return n(!1); var c = localStorage.getItem("font-index") || !1; if (c !== !1) try { c = JSON.parse(c) } catch (s) { c = !1 } return e._fontInjector = function (e) { if (e) { for (var t = Object.keys(e), n = [], r = function (e) { var t = e; try { t = JSON.parse(e) } catch (n) { return !1 } return t }, o = 0; o < t.length; o++) { var a = t[o], f = e[a], i = document.createElement("STYLE"); i.type = "text/css"; var c = f.fontfamily, s = f.fontweight, u = f.fontstyle, d = [], l = Number(s); l || (l = s), s = l, d.push("@font-face {"), d.push('font-family: "' + c + '";'), d.push("font-weight: " + s + ";"), d.push("font-style: " + u + ";"), d.push("src: "); var m = Object.keys(f.formats);[].forEach.call(m, function (e) { var t = f.formats[e], n = localStorage.getItem(t), o = r(n); "string" == typeof o && (o = r(o)), d.push("url(" + o.data + ') format("' + e + '")'), d.push(",") }), d[d.length - 1] = "", d.push(";"), i.innerHTML = d.join(""), n.push(i) } [].forEach.call(n, function (e) { document.head.appendChild(e) }) } }, c ? (e._fontInjector(c), document.querySelector("html").className += " fonts-ready", n(!0)) : n(!1) }(window);
    </script>
</head>
<!--<![endif]-->

 	<!--[if lt IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->