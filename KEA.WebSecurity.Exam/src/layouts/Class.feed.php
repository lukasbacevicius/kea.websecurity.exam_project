<?php

namespace WebSecurityExam;


class Feed
{
    
    public static $conf = array("db" => array(
        // Production
        "host" => "localhost",
        "port" => "3306",
        "username" => "kea_user",
        "password" => "j3diwannabe",
        "name" => "security_exam",
        "table" => "posts"
        // Local
        // "host" => "localhost", 
        // "port" => 8889, 
        // "username" => "security_exam", 
        // "password" => "tenn1s",
        // "name" => "security_exam",
        // "table" => "posts"
        ));
    
    public static $db = false;
    private static $dbh;
    
    
    public static function construct()
    {
        try {
            
            self::$dbh = new \PDO("mysql:dbname=" . self::$conf['db']['name'] . ";host=" . self::$conf['db']['host'] . ";port=" . self::$conf['db']['port'] . ";charset=utf8", self::$conf['db']['username'], self::$conf['db']['password']);
            
            self::$db = true;
            
        }
        catch (\PDOException $e) {
            
            \WebSecurityExam\Auth::log('Couldn\'t connect to database.');
            
            return false;
        }
    }
    
    public static function init()
    {
        self::construct();
    }
    
    public static function getFeed()
    {
        if (self::$db === true) {
            
            $sql = self::$dbh->prepare("SELECT * FROM `" . self::$conf['db']['table'] . "`");
            $sql->execute();
            
            return $sql->fetchAll(\PDO::FETCH_ASSOC);
            
        } else {
            return false;
        }
    }
    
    public static function post($author, $content, $filename, $filetemp, $filesize)
    {
        
        $file = $filename;
        
        $tmp_dir = $filetemp;
        
        $file_size = $filesize;
        
       	$text_content = strip_tags(trim($content));
        
        $date = date("Y-m-d H:i:s");

        if (empty($file) && empty($text_content)) {
            echo '<div class="large-4 columns large-centered"><p class="alert text-center callout">Can not submit empty form</p></div>';
            return;
        }
		 else if (empty($file)) {
            // TODO: handle image upload without text && text upload withoout images
            
            // Upload status
            $sql = self::$dbh->prepare('INSERT INTO posts(author_id, content, date_created) VALUES (:aid, :cont, :dd)');
            $sql->bindParam(':aid', $author);
            $sql->bindParam(':cont', $text_content);
            $sql->bindParam(':dd', $date);

            if ($sql->execute()) {
            	
                echo '<div class="large-4 columns large-centered"><p class="success text-center callout">Your post has been posted succesfully</p></div>';
                	
            } else {
                echo '<div class="large-4 columns large-centered"><p class="alert text-center callout">Status could not be posted</p></div>';
                return;
            }

        }  else {
            
            // Start image preparation

            $upload_dir = __DIR__ . "/uploads/";
            
            $imgExt = strtolower(pathinfo($file, PATHINFO_EXTENSION));


            
            $final_file = rand(1000, 1000000) . "." . $imgExt;
            

             $size = getimagesize($tmp_dir);

                if(!$size) {
                    echo $size;
                    echo '<div class="large-4 columns large-centered"><p class="alert text-center callout">Only images are allowed to upload</p></div>';
                    return;
                }   

                $valid_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP);
                
                if(in_array($size[2],  $valid_types)) {
                    if ($file_size < 2000000) {
                        move_uploaded_file($tmp_dir, $upload_dir . $final_file);
                    } else {
                        echo '<div class="large-4 columns large-centered"><p class="alert text-center callout">Your file is too large</p></div>';
                        return;
                    }
                } else {
                    
                    echo '<div class="large-4 columns large-centered"><p class="alert text-center callout">Sorry, only JPG, JPEG, PNG & GIF files are allowed</p></div>';
                    return;
                }
            
            // End image preparation
            
            $sql = self::$dbh->prepare('INSERT INTO posts(author_id, content, image, date_created) VALUES (:aid, :cont, :img, :dd)');
            $sql->bindParam(':aid', $author);
            $sql->bindParam(':cont', $text_content);
            $sql->bindParam(':img', $final_file);
			$sql->bindParam(':dd', $date);
            
            if ($sql->execute()) {

                echo '<div class="large-4 columns large-centered"><p class="success text-center callout">Your post has been posted succesfully</p></div>';
                
            } else {
                echo '<div class="large-4 columns large-centered"><p class="alert text-center callout">Status could not be posted</p></div>';
                return;
            }
            
        }

    }
}

?> 
