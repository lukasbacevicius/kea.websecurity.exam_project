<?php 
	
	namespace WebSecurityExam;

	class Auth {

		/*
			Begin configuration
		*/

		public static $conf = array(
			"db" => array(
			  // Production
			  "host" => "localhost",
			  "port" => "3306",
        	  "username" => "kea_user",
        	  "password" => "j3diwannabe",
			  "name" => "security_exam",
			  "table" => "users",
			  "token_table" => "resetTokens"
			   // Local
			   // "host" => "localhost",
			   // "port" => 8889,
			   // "username" => "security_exam",
			   // "password" => "tenn1s",
			   // "name" => "security_exam",
			   // "table" => "users",
			   // "token_table" => "resetTokens"
			),
			"keys" => array(
				"cookie" => "ckxc436jd*^30f840v*9!@#$",
				"salt" => "^#$4%9f+1^p9)M@4M)V$"
			),
			"brute_force" => array(
				"tries" => 5,
				"time_limit" => 300
			),
			"pages" => array(
			  "no_login" => array(
			    "/",
			    "/reset.php",
			    "/register.php"
			  ),
			  "login_page" => "/login.php",
			  "home_page" => "/home.php"
			),
			"cookies" => array(
				"expire" => "+30 days",
				"path" => "/",
				"domain" => ""
			),
			"two_step_login" => array(
				'instruction' => "Code has been sent to your email address. Type received code in the input box. (Note that email might have went to SPAM folder)",
				 'devices_table' => 'user_devices',
				 'token_length' => 4,
				 'numeric' => false,
				 'expire' => '+45 days'
			)
		);


		/* 
			End configuration
		*/

			private static $constructed = false;

		/*
			Logging
		*/

			public static function log($msg = ""){
			  $log_file = __DIR__ . "/Log.log";
			  if(file_exists($log_file)){
			    if($msg != ""){
			      $message = "[" . date("Y-m-d H:i:s") . "] $msg";
			      $fh = fopen($log_file, 'a');
			      fwrite($fh, $message . "\n");
			      fclose($fh);
			    }
			  }
			}

			public static $loggedIn = false;
			public static $db = true;
			public static $user = false;
			private static $init_called = false;
			private static $cookie;
			private static $session;
			private static  $remember_cookie;
			private static $dbh;

			public static function construct($called_from = ""){
			  if(self::$constructed === false){
			    self::$constructed = true;
			    session_start();
			    
			    /**
			    * Connecting to db
			    */
			    try{
			      array_push(self::$conf['pages']['no_login'], self::$conf['pages']['login_page']);
			      
			      self::$dbh = new \PDO("mysql:dbname=". self::$conf['db']['name'] .";host=". self::$conf['db']['host'] .";port=". self::$conf['db']['port']. ";charset=utf8", self::$conf['db']['username'], self::$conf['db']['password']);
			      self::$db = true;
			      
			      self::$cookie = isset($_COOKIE['authlogin']) ? $_COOKIE['authlogin'] : false;
			      self::$session = isset($_SESSION['authcuruser']) ? $_SESSION['authcuruser'] : false;
			      self::$remember_cookie = isset($_COOKIE['authrememberMe']) ? $_COOKIE['authrememberMe'] : false;
			      
			      $encUserID = hash("sha256", self::$conf['keys']['cookie'] . self::$session . self::$conf['keys']['cookie']);

			      if(self::$cookie == $encUserID){
			        self::$loggedIn = true;
			      }else{
			        self::$loggedIn = false;
			      }
			      
			      if(self::$remember_cookie !== false && self::$loggedIn === false){
			        $encUserID = hash("sha256", self::$conf['keys']['cookie']. self::$remember_cookie . self::$conf['keys']['cookie']);
			        if(self::$cookie == $encUserID){
			          self::$loggedIn = true;
			        }else{
			          self::$loggedIn = false;
			        }
			        
			        if(self::$loggedIn === true){
			          $_SESSION['authcuruser'] = self::$remember_cookie;
			          self::$session = self::$remember_cookie;
			        }
			      }

			      self::$user = self::$session;
			      
			      /*
			        Is device authorized
			       */
			      if(self::$loggedIn){
			        $login_page = self::curPage() === self::$conf['pages']['login_page'];

			        if(!isset($_COOKIE['authdevice']) && $login_page === false){
			          /**
			           	Logout if no device cookie
			           */
			          self::logout();
			          $called_from = "login";
			        }else if(!isset($_SESSION['device_check'])){
			          $sql = self::$dbh->prepare("SELECT '1' FROM `". self::$conf['two_step_login']['devices_table'] ."` WHERE `uid` = ? AND `token` = ?");
			          $sql->execute(array(self::$user, $_COOKIE['authdevice']));
			          
			          /**
			           	Remove cookie & logout as device is not authorized
			           */
			          if($sql->fetchColumn() !== '1' && $login_page === false){
			            setcookie("authdevice", "", time() - 10);
			            self::logout();
			            $called_from = "login";
			          }else{
			            $_SESSION['device_check'] = 1;
			          }
			        }
			      }
			      
			      if($called_from != "logout" && $called_from != "login"){
			        self::init();
			      }
			      return true;
			    }catch(\PDOException $e) {
			      /**
			       	Could not establish connection
			       */
			      self::log('Couldn\'t connect to database.');
			      return false;
			    }
			  }
			}

			public static function init() {
				self::construct();
				if(self::$loggedIn === true && array_search(self::curPage(), self::$conf['pages']['no_login']) !== false){
				  self::redirect(self::$conf['pages']['home_page']);
				}elseif(self::$loggedIn === false && array_search(self::curPage(), self::$conf['pages']['no_login']) === false){
				  self::redirect(self::$conf['pages']['login_page']);
				}
				self::$init_called = true;
			}

			public static function login($username, $password, $remember_me = false, $cookies = true) {
				self::construct("login");
				if(self::$db === true){

				 $query = "SELECT `id`, `password`, `password_salt`, `attempt` FROM `". self::$conf['db']['table'] ."` WHERE `username`=:login OR `email`=:login ORDER BY `id` LIMIT 1";
				  
				  $sql = self::$dbh->prepare($query);
				  $sql->bindValue(":login", $username);
				  $sql->execute();
				  
				  if($sql->rowCount() == 0){
				   	/* No such user */
				    return false;
				  }else{
				    /**
				     * Return user info
				     */
				    $rows = $sql->fetch(\PDO::FETCH_ASSOC);
				    $us_id = $rows['id'];
				    $us_pass = $rows['password'];
				    $us_salt = $rows['password_salt'];
				    $status = $rows['attempt'];
				    $saltedPass = hash('sha256', $password . self::$conf['keys']['salt'] . $us_salt);
				    
				    if(substr($status, 0, 2) == "b-"){
				      $blockedTime = substr($status, 2);
				      if(time() < $blockedTime){
				        $blocked = true;
				        return array(
				          "status"  => "blocked",
				          "minutes" => round(abs($blockedTime - time()) / 60, 0),
				          "seconds" => round(abs($blockedTime - time()) / 60*60, 2)
				        );
				      }else{
				        // time limit is over
				        self::updateUser(array(
				          "attempt" => "" // Zero tries
				        ), $us_id);
				      }
				    }
				    /**
				     * Password is empty
				     */
				    if(!isset($blocked) && ($saltedPass == $us_pass || $password == "")){
				      if($cookies === true){
				        
				        $_SESSION['authcuruser'] = $us_id;
				        
				        setcookie("authlogin", hash("sha256", self::$conf['keys']['cookie'] . $us_id . self::$conf['keys']['cookie']), strtotime(self::$conf['cookies']['expire']), self::$conf['cookies']['path'], self::$conf['cookies']['domain']);

				        if( $remember_me === true){
				          setcookie("authrememberMe", $us_id, strtotime(self::$conf['cookies']['expire']), self::$conf['cookies']['path'], self::$conf['cookies']['domain']);
				        }
				        self::$loggedIn = true;

				        self::updateUser(array(
				        "attempt" => "0"
				        ), $us_id);
				        
				        
				        // Redirect
				        if(self::$init_called){
				          self::redirect(self::$conf['pages']['home_page']);
				        }
				        return true;
				      }else{

				        return $us_id;
				      
				      }
				    }else{
				      /**
				       	 Incorrect password
				       */

				        $max_tries = self::$conf['brute_force']['tries'];
				        
				        if($status == ""){
				          // User was not logged in before
				          self::updateUser(array(
				            "attempt" => "1" // Tried 1 time
				          ), $us_id);
				        }else if($status == $max_tries){
				          /**
				           	Block account
				           */
				          $eligible_for_next_login_time = strtotime("+". self::$conf['brute_force']['time_limit'] ." seconds", time());
				          self::updateUser(array(
				            "attempt" => "b-" . $eligible_for_next_login_time
				          ), $us_id);
				          return array(
				            "status"  => "blocked",
				            "minutes" => round(abs($eligible_for_next_login_time - time()) / 60, 0),
				            "seconds" => round(abs($eligible_for_next_login_time - time()) / 60*60, 2)
				          );
				        }else if($status < $max_tries){
				          /*
							If the attempts are less than Max and not Max
				          */
				          self::updateUser(array(
				            "attempt" => $status + 1 
				          ), $us_id);
				        }
				      }
				      return false;
				    }
				  }
				}
		
		public static function register( $id, $password, $other = array() ){
		  self::construct();
		  if( self::userExists($id) || (isset($other['email']) && self::userExists($other['email'])) ){
		    return "exists";
		  }else{
		    $randomSalt  = self::rand_string(20);
		    $saltedPass  = hash('sha256', $password. self::$conf['keys']['salt'] . $randomSalt);
		    
		    if( count($other) == 0 ){
		      /* There's no other fields */
		      $sql = self::$dbh->prepare("INSERT INTO `". self::$conf['db']['table'] ."` (`username`, `password`, `password_salt`) VALUES(:username, :password, :passwordSalt)");
		    }else{
		      /* There is other fields */
		      $keys   = array_keys($other);
		      $columns = implode(",", $keys);
		      $colVals = implode(",:", $keys);
		      $sql   = self::$dbh->prepare("INSERT INTO `". self::$conf['db']['table'] ."` (`username`, `password`, `password_salt`, $columns) VALUES(:username, :password, :passwordSalt, :$colVals)");
		      foreach($other as $key => $value){
		        $value = htmlspecialchars($value);
		        $sql->bindValue(":$key", $value);
		      }
		    }
		    /* Bind def values */
		    $sql->bindValue(":username", $id);
		    $sql->bindValue(":password", $saltedPass);
		    $sql->bindValue(":passwordSalt", $randomSalt);
		    $sql->execute();
		    return true;
		  }
		}

		public static function logout(){
		  self::construct("logout");
		  session_destroy();
		  setcookie("authlogin", "", time() - 10, self::$conf['cookies']['path'], self::$conf['cookies']['domain']);
		  setcookie("authrememberMe", "", time() - 10, self::$conf['cookies']['path'], self::$conf['cookies']['domain']);
		 
		  usleep(2000);
		  self::redirect(self::$conf['pages']['login_page']);
		  return true;
		}

		public static function forgotPassword(){
		  self::construct();
		  $curStatus = "initial";  // Status of password recovery process
		  $identName = "Username or E-Mail";
		  
		  if( !isset($_POST['authForgotPass']) && !isset($_GET['resetPassToken']) && !isset($_POST['authForgotPassChange']) ){
		    $html = '<form action="'. self::curPageURL() .'" method="POST"><div class="row"><div class="large-5 columns large-centered"><div class="callout secondary">';
		      $html .= "<label>";
		        $html .= "{$identName}";
		        $html .= "<input type='text' id='authIdentification' placeholder='Enter your {$identName}' size='25' name='identification' />";
		      $html .= "</label>";
		      $html .= "<button class='button large text-center' name='authForgotPass' type='submit'>Reset Password</button>";
		    $html .= "</div></div></div></form>";
		    echo $html;
		    // update status
		    $curStatus = "resetPasswordForm";
		  }elseif( isset($_GET['resetPassToken']) && !isset($_POST['authForgotPassChange']) ){
		 	
		  	/*
				Check if token is valid
		  	*/

		    $reset_pass_token = urldecode($_GET['resetPassToken']);
		    $sql = self::$dbh->prepare("SELECT `uid` FROM `". self::$conf['db']['token_table'] ."` WHERE `token` = ?");
		    $sql->execute(array($reset_pass_token));
		    
		    if($sql->rowCount() == 0 || $reset_pass_token == ""){
		      echo '<div class="large-4 columns large-centered"><p class="alert callout">Wrong/Invalid Token</p></div>';
		      $curStatus = "invalidToken";
		    }else{
		      /**
		       * Token was valid
		       */
		      $html .= "<form action='{$_SERVER['PHP_SELF']}' method='POST'><div class='row'><div class='large-5 columns large-centered'><div class='callout secondary'>";
		        $html .= "<input type='hidden' name='token' value='{$reset_pass_token}' />";
		        $html .= "<label>";
		          $html .= "New Password";
		          $html .= "<input type='password' name='authForgotPassNewPassword' />";
		        $html .= "</label><br/>";
		        $html .= "<label>";
		          $html .= "Repeat Password";
		          $html .= "<input type='password' name='authForgotPassRetypedPassword'/>";
		        $html .= "</label>";
		        $html .= '<button class="button large text-center" name="authForgotPassChange">Reset Password</button>';
		      $html .= "</div></div></div></form>";
		      echo $html;
		   		
		   	  // Update status
		      $curStatus = "changePasswordForm";
		    }
		  }elseif(isset($_POST['authForgotPassChange']) && isset($_POST['authForgotPassNewPassword']) && isset($_POST['authForgotPassRetypedPassword'])){
		    $reset_pass_token = urldecode($_POST['token']);
		    $sql = self::$dbh->prepare("SELECT `uid` FROM `". self::$conf['db']['token_table'] ."` WHERE `token` = ?");
		    $sql->execute(array($reset_pass_token));
		    
		    if( $sql->rowCount() == 0 || $reset_pass_token == "" ){
		      echo '<div class="large-4 columns large-centered"><p class="alert callout">Wrong/Invalid Token</p></div>';
		      $curStatus = "invalidToken"; // The token user gave was not valid
		    }else{
		      if($_POST['authForgotPassNewPassword'] == "" || $_POST['authForgotPassRetypedPassword'] == ""){
		        echo '<div class="large-4 columns large-centered"><p class="alert callout">Passwords left blank</p></div>';
		        $curStatus = "fieldsLeftBlank";
		      }elseif( $_POST['authForgotPassNewPassword'] != $_POST['authForgotPassRetypedPassword'] ){
		        echo '<div class="large-4 columns large-centered"><p class="alert callout">Passwords do not match</p></div>';;
		        $curStatus = "passwordDontMatch"; // New password form invalid
		      }else{

		        self::$user = $sql->fetchColumn();
		        self::$loggedIn = true;
		        
		        if(self::changePassword($_POST['authForgotPassNewPassword'])){
		          self::$user = false;
		          self::$loggedIn = false;
		          
		          /**
		           * Remove token so it can not be used again
		           */
		          $sql = self::$dbh->prepare("DELETE FROM `". self::$conf['db']['token_table'] ."` WHERE `token` = ?");
		          $sql->execute(array($reset_pass_token));
		          
		             echo '<div class="large-4 columns large-centered"><p class="success callout">Password was reset</p></div>';
		          $curStatus = "passwordChanged"; 
		        }
		      }
		    }
		  }elseif(isset($_POST['identification'])){
		    /*
		      Check if identification is valid
		     */
		    $identification = $_POST['identification'];
		    if($identification == ""){
		        echo '<div class="large-4 columns large-centered"><p class="alert callout">{$identName} not provided</p></div>';
		      $curStatus = "identityNotProvided"; 
		    }else{
		      $sql = self::$dbh->prepare("SELECT `email`, `id` FROM `". self::$conf['db']['table'] ."` WHERE `username`=:login OR `email`=:login");
		      $sql->bindValue(":login", $identification);
		      $sql->execute();
		      if($sql->rowCount() == 0){
		        echo '<div class="large-5 columns large-centered"><p class="alert callout">User Not Found</p></div>';
		        $curStatus = "userNotFound";
		      }else{
		        $rows  = $sql->fetch(\PDO::FETCH_ASSOC);
		        $email = $rows['email'];
		        $uid   = $rows['id'];
		        
		        /*
		         	New token
		         */
		        $token = self::rand_string(40);
		        $sql = self::$dbh->prepare("INSERT INTO `". self::$conf['db']['token_table'] ."` (`token`, `uid`, `requested`) VALUES (?, ?, NOW())");
		        $sql->execute(array($token, $uid));
		        $encodedToken = urlencode($token);
		        
		        /*
		         	Email
		         */
		        $subject = "Reset Password";
		        $body   = "Reset password link :
		        <blockquote>
		          <a href='". self::curPageURL() ."?resetPassToken={$encodedToken}'>Reset Password : {$token}</a>
		        </blockquote>";
		        self::sendMail($email, $subject, $body);
		        
		        echo '<div class="large-5 columns large-centered"><p class="success callout">An email has been sent to your email inbox with instructions.</p></div>';
		        $curStatus = "emailSent"; // E-Mail has been sent
		      }
		    }
		  }
		  return $curStatus;
		}

		public static function changePassword($newpass){
		  self::construct();
		  if(self::$loggedIn){
		    $randomSalt = self::rand_string(20);
		    $saltedPass = hash('sha256', $newpass . self::$conf['keys']['salt'] . $randomSalt);
		    $sql = self::$dbh->prepare("UPDATE `". self::$conf['db']['table'] ."` SET `password` = ?, `password_salt` = ? WHERE `id` = ?");
		    $sql->execute(array($saltedPass, $randomSalt, self::$user));
		    return true;
		  }else{
		      echo '<div class="large-5 columns large-centered"><p class="alert callout">Not logged in</p></div>';
		    return "notLoggedIn";
		  }
		}

		public static function userExists($identification){
		  self::construct();
		  $query = "SELECT `id` FROM `". self::$conf['db']['table'] ."` WHERE `username`=:login OR `email`=:login";
		  
		  $sql = self::$dbh->prepare($query);
		  $sql->execute(array(
		    ":login" => $identification
		  ));
		  return $sql->rowCount() == 0 ? false : true;
		}


		public static function getUser($what = "*", $user = null){
		  self::construct();
		  if($user == null){
		    $user = self::$user;
		  }
		  if( is_array($what) ){
		    $columns = implode("`,`", $what);
		    $columns  = "`{$columns}`";
		  }else{
		    $columns = $what != "*" ? "`$what`" : "*";
		  }
		  
		  $sql = self::$dbh->prepare("SELECT {$columns} FROM `". self::$conf['db']['table'] ."` WHERE `id` = ? ORDER BY `id` LIMIT 1");
		  $sql->execute(array($user));
		  
		  $data = $sql->fetch(\PDO::FETCH_ASSOC);
		  if( !is_array($what) ){
		    $data = $what == "*" ? $data : $data[$what];
		  }
		  return $data;
		}

		public static function updateUser($toUpdate = array(), $user = null){
		  self::construct();
		  if( is_array($toUpdate) && !isset($toUpdate['id']) ){
		    if($user == null){
		      $user = self::$user;
		    }
		    $columns = "";
		    foreach($toUpdate as $k => $v){
		      $columns .= "`$k` = :$k, ";
		    }
		    $columns = substr($columns, 0, -2);
		  
		    $sql = self::$dbh->prepare("UPDATE `". self::$conf['db']['table'] ."` SET {$columns} WHERE `id`=:id");
		    $sql->bindValue(":id", $user);
		    foreach($toUpdate as $key => $value){
		      $value = htmlspecialchars($value);
		      $sql->bindValue(":$key", $value);
		    }
		    $sql->execute();
		    
		  }else{
		    return false;
		  }
		}

		public static function twoStepLogin($identification = "", $password = "", $remember_me = false){
		  if(isset($_POST['auth_two_step_login-token']) && isset($_POST['auth_two_step_login-uid']) && $_SESSION['auth_two_step_login-first_step'] === '1'){

		    $uid = $_POST['auth_two_step_login-uid'];
		    $token = $_POST['auth_two_step_login-token'];
		    
		    $sql = self::$dbh->prepare("SELECT COUNT(1) FROM `". self::$conf['db']['token_table'] ."` WHERE `token` = ? AND `uid` = ?");
		    $sql->execute(array($token, $uid));
		    
		    if($sql->fetchColumn() == 0){

		      $_SESSION['auth_two_step_login-first_step'] = '0';
		      echo "<h3>Error : Wrong/Invalid Token</h3>";
		      return "invalidToken";
		    }else{

		      if(isset($_POST['auth_two_step_login-dontask'])){
		        $device_token = self::rand_string(10);
		        $sql = self::$dbh->prepare("INSERT INTO `". self::$conf['two_step_login']['devices_table'] ."` (`uid`, `token`, `last_access`) VALUES (?, ?, NOW())");
		        $sql->execute(array($uid, $device_token));
		        setcookie("authdevice", $device_token, strtotime(self::$conf['two_step_login']['expire']), self::$conf['cookies']['path'], self::$conf['cookies']['domain']);
		      }
		      
		      /*
		        Remove token so it can not be reused
		       */
		      $sql = self::$dbh->prepare("DELETE FROM `". self::$conf['db']['token_table'] ."` WHERE `token` = ? AND `uid` = ?");
		      $sql->execute(array($token, $uid));
		      self::login(self::getUser("username", $uid), "", isset($_POST['auth_two_step_login-remember_me']));
		    }
		    return true;
		  }else if($identification != "" && $password != ""){
		    $login = self::login($identification, $password, $remember_me, false);
		    if($login === false){
		      /*
		        Username/Password wrong
		       */
		      return false;
		    }else if(is_array($login) && $login['status'] == "blocked"){
		      return $login;
		    }else{
		  
		      $uid = $login;
		        
		      /**
		       * If device is verified multilevel login can be skipped
		       */

		      if(isset($_COOKIE['authdevice'])){
		        $sql = self::$dbh->prepare("SELECT 1 FROM `". self::$conf['two_step_login']['devices_table'] ."` WHERE `uid` = ? AND `token` = ?");
		        $sql->execute(array($uid, $_COOKIE['authdevice']));
		        if($sql->fetchColumn() == "1"){
		          $verfied = true;
		          /**
		           * Update last accessed time
		           */
		          $sql = self::$dbh->prepare("UPDATE `". self::$conf['two_step_login']['devices_table'] ."` SET `last_access` = NOW() WHERE `uid` = ? AND `token` = ?");
		          $sql->execute(array($uid, $_COOKIE['authdevice']));
		          
		          self::login(self::getUser("username", $uid), "", $remember_me);
		          return true;
		        }
		      }

		      if(!isset($verified)){
		        
		        $_SESSION['auth_two_step_login-first_step'] = '1';
		        
		
		        $token = self::rand_string(self::$conf['two_step_login']['token_length'], self::$conf['two_step_login']['numeric']);
		        
		        $sql = self::$dbh->prepare("INSERT INTO `". self::$conf['db']['token_table'] ."` (`token`, `uid`, `requested`) VALUES (?, ?, NOW())");
		        $sql->execute(array($token, $uid));
		        
		        call_user_func_array(function($userID, $token) {
				   $email = \WebSecurityExam\Auth::getUser("email", $userID);
				   
				   \WebSecurityExam\Auth::sendMail($email, "Verify Yourself", ": <blockquote>". $token ."</blockquote>");
				 }, array($uid, $token));
		
		        $html = "<form action='". self::curPageURL() ."' method='POST'>
		          <div class='large-5 columns large-centered'><p class='success callout'>". self::$conf['two_step_login']['instruction'] ."</p></div><div class='row'><div class='large-5 columns large-centered'><div class='callout secondary'>
		          <label>
		            Token Received
		            <input type='text' name='auth_two_step_login-token' placeholder='Paste the code here... (case sensitive)' />
		          </label>
		          <label>
		            Remember this device ?
		            <input type='checkbox' name='auth_two_step_login-dontask' />
		          </label>
		          <input type='hidden' name='auth_two_step_login-uid' value='". $uid ."' />
		          ". ($remember_me === true ? "<input type='hidden' name='auth_two_step_login-remember_me' />" : "") ."
		          
		            <button class='button large text-center'>Verify</button>
		          
		        </div></div></div></form>";
		        echo $html;
		        return "formDisplay";
		      }else{
		        self::log("two_step_login: Token Callback not present");
		      }
		    }
		  }
		  return false;
		}

		public static function getDevices(){
		  if(self::$loggedIn){
		    $sql = self::$dbh->prepare("SELECT * FROM `". self::$conf['two_step_login']['devices_table'] ."` WHERE `uid` = ?");
		    $sql->execute(array(self::$user));
		    return $sql->fetchAll(\PDO::FETCH_ASSOC);
		  }else{
		    return false;
		  }
		}

		public static function revokeDevice($device_token){
		  if(self::$loggedIn){
		    $sql = self::$dbh->prepare("DELETE FROM `". self::$conf['two_step_login']['devices_table'] ."` WHERE `uid` = ? AND `token` = ?");
		    $sql->execute(array(self::$user, $device_token));
		    if(isset($_SESSION['device_check'])){
		      unset($_SESSION['device_check']);
		    }
		    return $sql->rowCount() == 1;
		  }
		}

		public static function validEmail($email = ""){
		  return filter_var($email, FILTER_VALIDATE_EMAIL);
		}

		public static function curPageURL() {
		  $pageURL = 'http';
		  if(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on"){$pageURL .= "s";}
		  $pageURL .= "://";
		  if($_SERVER["SERVER_PORT"] != "80") {
		    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		  }else{
		    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		  }
		  return $pageURL;
		}

		public static function rand_string($length, $int = false) {
		  $random_str = "";
		  $chars = $int ? "0516243741506927589" : "subinsblogabcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		  $size = strlen($chars) - 1;
		  for($i = 0;$i < $length;$i++) {
		    $random_str .= $chars[rand(0, $size)];
		  }
		  return $random_str;
		}

		public static function curPage(){
		  $parts = parse_url(self::curPageURL());
		  return $parts["path"];
		}

		public static function redirect($url, $status = 302){
		  header("Location: $url", true, $status);
		  exit;
		}

		public static function sendMail($email, $subject, $body){
		  if(is_callable(0)){
		    call_user_func_array(0, array($email, $subject, $body));
		    
		  }else{
		    $headers = array();
		    $headers[] = "MIME-Version: 1.0";
		    $headers[] = "Content-type: text/html; charset=iso-8859-1";
		    $headers[] = "From: lukasBacevicius@Group14.com";
		    $headers[] = "Reply-To: lukasBacevicius@Group14.com";
		    mail($email, $subject, $body, implode("\r\n", $headers));

		  }
	

		}

		public static function csrf($type = ""){
		  if(!isset($_COOKIE['csrf_token'])){
		    $csrf_token = self::rand_string(5);
		    setcookie("csrf_token", $csrf_token, 0, self::$conf['cookies']['path'], self::$conf['cookies']['domain']);
		  }else{
		    $csrf_token = $_COOKIE['csrf_token'];
		  }
		  if($type == "s"){
		    return urlencode($csrf_token);
		  }elseif($type == "g"){

		    return "&csrf_token=" . urlencode($csrf_token);
		  }elseif($type == "i"){

		    echo "<input type='hidden' name='csrf_token' value='{$csrf_token}' />";
		  }else{

		    if((isset($_POST['csrf_token']) && $_COOKIE['csrf_token'] == $_POST['csrf_token']) || (isset($_GET['csrf_token']) && $_COOKIE['csrf_token'] == $_GET['csrf_token'])){
		      return true;
		    }else{

		      return false;
		    }
		  }
		}

	}

?>