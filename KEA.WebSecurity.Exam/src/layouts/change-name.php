		<div class="row">
			<div class="large-5 columns large-centered">
				<div class="callout small">
					<h3>Your current name:</h3>
					<p><strong>
						<?php
						$details = \WebSecurityExam\Auth::getUser();
						print_r($details['name']);
						?>
					</strong>
					</p>
				</div>
			</div>
		</div>

		<form action="home.php" method="POST">
			<div class="row">
			<div class="large-5 columns large-centered">
			<div class="callout secondary">
			<label>
				Change your account name :
			<input type="text" name="newName" placeholder="New name" />
			</label>
			<button class="button large text-center" disabled>Change Name</button>
			  </div>
			</div>
			</div>
		</form>