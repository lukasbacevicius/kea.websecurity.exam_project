var PATH = {};


/******************************************
*   INPUT PATHS
*******************************************/
PATH.src = {};
PATH.src.dir = ['./deploy'];
PATH.src.sass = ['./src/sass/**/*.scss'];
PATH.src.js = ['./src/js/**/*.js'];
PATH.src.images = ['./src/images/**/*.jpg', './src/images/**/*.png', './src/images/**/*.gif', './src/images/**/*.svg', './src/images/**/*.ico'];
PATH.src.fonts = ['./src/fonts/*.eot', './src/fonts/*.svg', './src/fonts/*.ttf', './src/fonts/*.woff'];
PATH.src.layouts = ['./src/layouts/*.html', './src/layouts/*.aspx', , './src/layouts/*.php'];
PATH.src.layoutsWatch = ['./src/layouts/**/*'];
PATH.src.rootfiles = ['./src/js/serviceworker.js'];


/******************************************
*   OUTPUT PATHS
*******************************************/
PATH.project_name = "web-security-exam-project";
PATH.css_build_dirs = ['./deploy/assets/' + PATH.project_name + '/css'];
PATH.css_build_name = ['app']; // must be the same, ie.: src/sass/app.scss
PATH.js_build_dirs = ['./deploy/assets/' + PATH.project_name + '/js'];
PATH.js_build_name = ['app']; // must be the same, ie.: src/js/app.js
PATH.images_build_dirs = ['./deploy/assets/' + PATH.project_name + '/images'];
PATH.fonts_build_dirs = ['./deploy/assets/' + PATH.project_name + '/fonts'];
PATH.layouts_build_dirs = ['./deploy'];
PATH.rootfiles_build_dirs = ['./deploy'];


/******************************************
*   CONFIG FILES
*******************************************/
var CONFIG = {};
CONFIG.fonts = ['./src/fonts.json'];
CONFIG.fontDirectory = './deploy/assets/fonts';


/******************************************
*   DEPENDENCIES
*******************************************/
var fs = require('fs');
var exec = require('child_process').exec;
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var include = require('gulp-include');
var rename = require('gulp-rename');
var ignore = require('gulp-ignore');
var rimraf = require('gulp-rimraf');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var minifyCSS = require('gulp-clean-css');
var merge = require('merge-stream');
var runSequence = require('run-sequence');
var autoprefixer = require('gulp-autoprefixer');
var chmod = require('gulp-chmod');
var importCss = require('gulp-import-css');
var entityconvert = require('gulp-entity-convert');
var stripBom = require('gulp-stripbom');
var bower = require('gulp-bower');


/******************************************
*   CLEAN
*******************************************/
gulp.task('clean', function () {
    cache.clearAll();
    return gulp.src(PATH.src.dir, { read: false }).pipe(rimraf({ force: true }));
});


/******************************************
*   FONTS
*******************************************/
gulp.task('fonts', ['fonts_raw'], function() {

    function string_stream(filename, string) {
      var src = require('stream').Readable({ objectMode: true })
      src._read = function () {
        this.push(new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(string) }))
        this.push(null)
      }
      return src
    }


    var fontCollection = [];

    CONFIG.fonts.forEach(function(fontConfig) {

        fs.readFile(fontConfig, function(err, data) {

            //read fontConfig
            if (err) {
                return console.log(err);
            }

            var cssCollected = [];
            var fontPaths = [];

            var fontPromise = new Promise(function(resolver, rejector) {

                //create main promise
                //promise returns gathered css

                var fontSubPromises = [];

                //parse font object
                var fontObj = JSON.parse(data.toString());

                //create css array

                fontObj.forEach(function(font) {

                    var fontFamily = font.fontfamily,
                        fontWeight = font.fontweight,
                        fontStyle = font.fontstyle,
                        fontVersion = font.version;
                    var formats = font.src,
                        formatKeys = Object.keys(formats);


                    if (!fontFamily || !formats) {
                        console.log('Either no fontfamily or src');
                        return;
                    }

                    var css = [];

                    css.push('@font-face {');
                    css.push('font-family: "'+fontFamily+'";');
                    css.push('font-weight: '+fontWeight+';');
                    css.push('font-style: '+fontStyle+';');
                    css.push('src: ');


                    var fontPathPush = {
                        fontfamily: fontFamily,
                        fontweight: fontWeight,
                        fontstyle: fontStyle,
                        fontversion: fontVersion,
                        formats: {}
                    };


                    for (var i = 0; i < formatKeys.length; i++) {
                        if (formatKeys[i] !== 'woff') {
                          continue;
                        }
                        var subPromise = new Promise(function(subresolver, subrejector) {
                            var key = formatKeys[i],
                                src = formats[key];

                            if (!key) {
                                subrejector(key);
                                return;
                            }

                            var fontData = [];
                            var output = {};

                            var isLast = i+1 >= formatKeys.length;


                            fs.readFile(src, function(err, data) {
                                if (err) {
                                    subrejector(err);

                                    return console.log(err);
                                }

                                var baseEncoded = data.toString('base64');

                                fontData.push('data:application/font-');
                                fontData.push(key);
                                fontData.push(';base64,');
                                fontData.push(baseEncoded);
                                // output.fontfamily = fontFamily;
                                // output.fontweight = fontWeight || '';
                                // output.fontstyle = fontStyle || '';
                                // output.format = key;
                                output.data = fontData.join('');
                                output.version = fontVersion;

                                if (key == 'ttf') {
                                  css.push('url('+fontData.join('')+') format("truetype")');
                                } else if (key == 'eot') {
                                  css.push('url('+fontData.join('')+') format("embedded-opentype")');
                                } else {
                                  css.push('url('+fontData.join('')+') format("'+key+'")');
                                }

                                css.push(',');

                                cssCollected.push(css);
                                // RESOLVE


                                PATH.fonts_build_dirs.forEach(function(dir) {
                                    var writeFile = fontFamily+'.'+fontWeight+'.'+fontStyle+'.'+key+'.json',
                                        writePath = dir+'/';
                                    var saved = true;
                                    var writeReference = CONFIG.fontDirectory+'/'+writeFile;

                                    try {
                                        fs.writeFileSync(writePath+writeFile, JSON.stringify(output), 'utf8');
                                    } catch(e) {
                                        saved = false;
                                        console.log(e);
                                    }

                                    if (saved) {
                                        var found = 0;
                                        [].forEach.call(fontPaths, function(path) {
                                            if (path.path == writeReference) {
                                                found++;
                                            }
                                        });
                                        if (found == 0) {

                                            fontPathPush.formats[key] = writeReference;

                                            // fontPaths.push({
                                            //     'path': writeReference
                                            // });
                                        }
                                        subresolver(css);
                                    } else {
                                        subrejector();
                                    }

                                });

                            });
                        });
                      
                        fontSubPromises.push(subPromise);

                    };

                    fontPaths.push(fontPathPush);

                });

                Promise.all(fontSubPromises).then(function(res) {

                  [].forEach.call(cssCollected, function(collection, i) {
                    collection[collection.length-1] = ';}';
                    cssCollected[i] = collection.join('');
                  });


                  var cssText = cssCollected.join('');
                  resolver(cssText);
                }).catch(function(rej) {
                  console.log(rej);
                });


            });


            fontPromise.then(function(data, err) {

                PATH.css_build_dirs.forEach(function(dir) {
                    var saved = true;
                    var saveTo =  dir+'/fonts.css';

                    try {
                        fs.writeFileSync(saveTo, data, 'utf8');
                    } catch(e) {
                        saved = false;
                        console.log(e);
                    }

                    if (!saved) {
                        console.log('could not save',  saveTo)
                    }
                });

                PATH.js_build_dirs.forEach(function(dir) {
                    var saved = true;
                    var saveTo = dir+'/fonts.json';

                    try {
                        fs.writeFileSync(saveTo, JSON.stringify(fontPaths), 'utf8');
                    } catch(e) {
                        saved = false;
                        console.log(e);
                    }

                    if (!saved) {
                        console.log('could not save', saveTo);
                    }
                })
            });

        });

    });

});

/******************************************
*   STYLES
*******************************************/
gulp.task('styles', function () {
    var _merge = new merge();
    PATH.css_build_dirs.forEach(function (dir) {
		for (var i = 0; i < PATH.css_build_name.length; i++) {
			_merge.add(gulp.src('./src/sass/' + PATH.css_build_name[i] + '.scss')
				.pipe(sass({ outputStyle: 'nested', errLogToConsole: true, includePaths: ['./bower_components/foundation-sites/scss', './bower_components/motion-ui/src'] }).on('error', sass.logError))
				.pipe(importCss())
				.pipe(autoprefixer({ browsers: ['last 4 versions'], cascade: true, remove: true }))
				.pipe(chmod(777))
				.pipe(gulp.dest(dir)));
		}        
    });
    return _merge;
});
gulp.task('styles:min', ['styles'], function () {
    var _merge = new merge();
    PATH.css_build_dirs.forEach(function (dir) {
		for (var i = 0; i < PATH.css_build_name.length; i++) {
			_merge.add(gulp.src(dir + '/' + PATH.css_build_name[i] + '.css')
				.pipe(minifyCSS({ processImportFrom: ['!fonts.googleapis.com'] }))
				.pipe(rename({ suffix: '.' + uuid }))
				.pipe(gulp.dest(dir)));
		}
    });
    return _merge;
});


/******************************************
*   SCRIPTS
*******************************************/
gulp.task('scripts', function(){
	var _merge = new merge();
	PATH.js_build_dirs.forEach(function (dir) {
		for (var i = 0; i < PATH.js_build_name.length; i++) {
			_merge.add(gulp.src('./src/js/' + PATH.js_build_name[i] + '.js')
				.pipe(include())
				.pipe(chmod(777))
				.pipe(gulp.dest(dir)));
		}
	});
	return _merge;
});
gulp.task('scripts:min', ['scripts'], function () {
    var _merge = new merge();
    PATH.js_build_dirs.forEach(function (dir) {
		for (var i = 0; i < PATH.js_build_name.length; i++) {
			_merge.add(gulp.src(dir + '/' + PATH.js_build_name[i] + '.js')
			.pipe(uglify())
			.pipe(rename({ suffix: '.' + uuid }))
			.pipe(gulp.dest(dir)));
		}
    });
    return _merge;
});


/******************************************
*   IMAGES
*******************************************/
gulp.task('images', function () {
    var _merge = new merge();
    PATH.images_build_dirs.forEach(function (dir) {
        _merge.add(gulp.src(PATH.src.images)
          .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
		  .pipe(chmod(777))
          .pipe(gulp.dest(dir)));
    });
    return _merge;
});


/******************************************
*   FONTS
*******************************************/
gulp.task('fonts_raw', function () {
    var _merge = new merge();
    PATH.fonts_build_dirs.forEach(function (dir) {
        _merge.add(gulp.src(PATH.src.fonts)
		.pipe(chmod(777))
        .pipe(gulp.dest(dir)));
    });
    return _merge;    
});


/******************************************
*   LAYOUTS
*******************************************/
gulp.task('layouts', function () {
    var _merge = new merge();
    PATH.layouts_build_dirs.forEach(function (dir) {
        _merge.add(gulp.src(PATH.src.layouts)
            .pipe(include())
			.pipe(stripBom())
			.pipe(entityconvert())
			.pipe(chmod(777))
	        .pipe(gulp.dest(dir)));
    });
    return _merge;
});

/******************************************
*   ROOT FILES
*******************************************/

gulp.task('rootfiles', function() {
  var _merge = new merge();

  PATH.rootfiles_build_dirs.forEach(function(buildDir) {


    PATH.src.rootfiles.forEach(function(rootfile) {
      _merge.add(gulp.src(rootfile)
        .pipe(include())
        .pipe(gulp.dest(buildDir)));

      _merge.add(gulp.src(rootfile)
        .pipe(include())
        .pipe(gulp.dest(buildDir)));
    });

  });

  return _merge;
});

gulp.task('rootfiles:min', function() {
  var _merge = new merge();

  PATH.rootfiles_build_dirs.forEach(function(buildDir) {

    PATH.src.rootfiles.forEach(function(rootfile) {
      var firstFn = null;
      if (rootfile.indexOf('.js') > -1 || rootfile.indexOf('.json') > -1) {
        firstFn = function() {
          return uglify()
        };
      } else if (rootfile.indexOf('.css') > -1) {
        firstFn = function() {
          return minifyCSS({ processImportFrom: ['!fonts.googleapis.com'] })
        };
      }

      _merge.add(gulp.src(rootfile)
        .pipe(include())
        .pipe(firstFn())
        .pipe(gulp.dest(buildDir)));
    });
  });

  return _merge;
});


/******************************************
*   CACHE BUSTING
*******************************************/
var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var d = new Date().getTime();
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
});
gulp.task('bust', function () {
    PATH.js_build_dirs.forEach(function (dir) {
        fs.writeFile(dir + '/bust.json', '{"id":"' + uuid + '"}', function (err) {
            if (err) throw err;
            console.log('Bust saved to: ' + dir);
        });
    });
});


/******************************************
*   TASKS
*******************************************/
gulp.task('build', function (callback) {
    runSequence('clean', ['scripts', 'styles', 'rootfiles', 'images', 'fonts', 'layouts'], callback);
});
gulp.task('deploy', function (callback) {
    runSequence('bower', 'build', ['styles:min', 'rootfiles:min', 'scripts:min', 'bust'], callback);
});
gulp.task('default', ['build']);

gulp.task('watch', ['clean', 'build'], function () {
    gulp.watch(PATH.src.sass, ['styles']);
    gulp.watch(PATH.src.js, ['scripts']);
    gulp.watch(PATH.src.images, ['images']);
    gulp.watch(PATH.src.fonts, ['fonts']);
	gulp.watch(PATH.src.layoutsWatch, ['layouts']);
});


/******************************************
*   OTHER STUFF. ie. independent install tasks, etc.
*******************************************/
gulp.task('postinstall', function (callback) {
	runSequence('bower', 'install_foundation_settings', callback);
});
gulp.task('bower', function () {
    return bower();
});
gulp.task('install_foundation_settings', function () {
    fs.stat('./src/sass/_settings.scss', function (err, stat) {
        if (err == null) {
            console.log("File _settings.scss exists. Don't copy.");
        } else {
            gulp.src(['./bower_components/foundation-sites/scss/settings/_settings.scss']).pipe(gulp.dest('./sass'));
        }
    });	
});