<?php 
	include "header.php";
?>
	
	<div class="callout">
	  <div class="row column text-center">
	      <h1>Reset password</h1>
	  </div>
	</div>

<?php
	\WebSecurityExam\Auth::forgotPassword();
?>
	<div class="row">
  	<div class="large-10 large-centered columns text-center">
		<a class="button large hollow" href="login.php">Back to login page</a>
	</div>
	</div>
<?php include "footer.php"; ?>


