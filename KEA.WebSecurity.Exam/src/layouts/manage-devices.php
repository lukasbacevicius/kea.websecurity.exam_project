<?php 
  
  include "header.php";
  
	if(isset($_GET['revoke_device']) && \WebSecurityExam\Auth::csrf()){
	  if(\WebSecurityExam\Auth::revokeDevice($_GET['revoke_device'])){
	    $revoked = true;
	  }else{
	    $revoked = false;
	  }
	}
?>
<div class="callout">
  <div class="row column text-center">
  <h1>Manage devices</h1>
  <p class="lead">
      Device authorized to use your account with
  </p>
  </div>
</div>

  <?php
  if(isset($revoked)){
    if($revoked){
      echo '<div class="large-7 columns large-centered"><p class="success callout">Successfully Revoked Device</p></div>';
    }else{
       echo '<div class="large-7 columns large-centered"><p class="success callout">Failed to revoke device</p></div>';
    }
  }
  $devices = \WebSecurityExam\Auth::getDevices();
  if(count($devices) == 0){
    echo '<div class="large-7 columns large-centered"><p class="secondary callout">No devices</p></div>';
  }else{
    echo "<div class='row'><div class='large-7 columns large-centered'><table border='1' cellpadding='10px'>
    <thead>
      <th>ID</th>
      <th>Last Accessed</th>
      <th></th>
    </thead>
    <tbody>";
    foreach($devices as $device){
      echo "<tr>
        <td>{$device['token']}</td>
        <td>{$device['last_access']}</td>
        <td><a href='?revoke_device={$device['token']}" . \WebSecurityExam\Auth::csrf("g") ."'>Revoke Access</a></td>
      </tr>";
    }
    echo "</tbody></table></div></div>";
  }
  ?>

    <div class="row">
      <div class="large-10 large-centered columns text-center">
      <a class="button large hollow" href="home.php">Back to home page</a>
    </div>
    </div>


<?php include "footer.php"; ?>